# MicroShiftのインストール

## 事前準備
```
/bin/bash
su -
. /etc/os-release
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/Release.key" | sudo apt-key add -
apt -y update
```


`/boot/cmdline.txt`の末尾に以下を追記し、リブートしてください。

```
vi /boot/cmdline.txt
```

```
cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
```

## 必要なパッケージ類をインストール
```
apt install -y curl jq runc iptables conntrack
```

## CRI-O(コンテナランタイム)をインストール
```
curl https://raw.githubusercontent.com/cri-o/cri-o/release-1.21/scripts/get | bash
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.21.0/crictl-v1.21.0-linux-arm64.tar.gz
tar zxvf crictl-v1.21.0-linux-arm64.tar.gz
mv crictl /usr/local/bin
rm crictl-v1.21.0-linux-arm64.tar.gz
```

## MicroShift(AIO: All-In-One)のインストール
```
curl -LO https://github.com/openshift/microshift/releases/download/nightly/microshift-linux-arm64
mv microshift-linux-arm64 /usr/local/bin/microshift; chmod 755 /usr/local/bin/microshift
```

## MicroShiftをsystemdで管理できるように設定
```
cat << EOF > /usr/lib/systemd/system/microshift.service
[Unit]
Description=MicroShift
After=crio.service

[Service]
WorkingDirectory=/usr/local/bin/
ExecStart=/usr/local/bin/microshift run
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOF
```

## CRI-OとMicroShiftを起動
```
systemctl enable crio --now
systemctl enable microshift.service --now
```

## MicroShiftのCLIツール(ocコマンド)をインストール
```
curl -LO https://mirror.openshift.com/pub/openshift-v4/arm64/clients/ocp/stable/openshift-client-linux.tar.gz
tar xvf openshift-client-linux.tar.gz
chmod +x oc
mv oc /usr/local/bin
rm openshift-client-linux.tar.gz 
export KUBECONFIG=/var/lib/microshift/resources/kubeadmin/kubeconfig
```

## インストール後の自動セットアップの状況を確認
```
oc get nodes
```

```
[実行結果]
NAME               STATUS   ROLES    AGE   VERSION
microshift.local   Ready    <none>   42m   v1.21.0
```
> Nodeの状態が「Ready」であることを確認

```
oc get po -A
```

```
[実行結果]
NAMESPACE                       NAME                                  READY   STATUS    RESTARTS
kube-system                     kube-flannel-ds-gzzr8                 1/1     Running   1       
kubevirt-hostpath-provisioner   kubevirt-hostpath-provisioner-vzdrw   1/1     Running   1       
openshift-dns                   dns-default-9wmv5                     2/2     Running   2       
openshift-dns                   node-resolver-m6jnj                   1/1     Running   1       
openshift-ingress               router-default-85bcfdd948-n8thj       1/1     Running   1       
openshift-service-ca            service-ca-7764c85869-8b7j5           1/1     Running   2       
```

## mDNSの設定
MicroShiftは、mDNSの機能がビルトインされてるため、同一LAN上のホストからコンテナへ`.local`ドメインでアクセスできます。
ホスト側に以下の流れで`avahi`をインストールし、MicroShift上のコンテナの名前解決をできるようにしましょう。

### avahi-daemonのインストール
```
apt  install -y avahi-daemon
```

### avahi-daemonの起動
```
systemctl enable avahi-daemon --now
```

### nsswitchの設定変更
```
sed -i 's/files dns/files mdns4_minimal [NOTFOUND=return] dns/g' /etc/nsswitch.conf
```

これで、同じLAN上にある他のホストから、Orinに対して`orin.local`のドメインで名前解決できるようになります。

```
# 別のホストからping
vi /etc/nsswitch.conf
hosts:          files mdns4_minimal [NOTFOUND=return] dns

$ ping orin.loal
PING microshift.local (192.168.2.104): 56 data bytes
64 bytes from 192.168.2.104: icmp_seq=0 ttl=64 time=8.822 ms
```