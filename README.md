# SunFoundar PiCar-V Integrates Kubernetes

# Getting Started
## STEP1. PiCar-Vを[公式ドキュメント](https://www.sunfounder.com/)に沿って組み立てます。

本リポジトリでは、Raspberry Pi4BをPiCar-Vに搭載しています。

## STEP2. [ドキュメント](./doc/microshift.md)に沿ってラズパイへMicroShiftをインストールします。

## STEP3. ラズパイ上でコンテナイメージをビルドします。

ここでは、ラズパイにRaspbian OS(64bit)がインストールされている想定で記載します。

### Raspbian OSのコンテナイメージをローカルにインポートします。
```
wget https://downloads.raspberrypi.org/raspios_lite_arm64/root.tar.xz
docker image import root.tar.xz raspios_lite_arm64:bullseye
```

### アプリケーションのコンテナイメージをビルドします。

```
git clone https://gitlab.com/yono1/picar-meets-k8s
cd picar-meets-k8s/src
podman build -t picar:1.0
```

## STEP4. MicroShiftへアプリケーションをデプロイします。

```
cd ..
oc apply -f manifest/demo1/
```

## STEP5. PiCar-Vの管理コンソールへアクセス

以下のURLへブラウザでアクセスし、管理コンソールが表示されることを確認します。

なお、PiCar-Vと同一LAN(WiFiなど)に接続されるPCからアクセスしてください。

> http://picar-web.cluster.local
